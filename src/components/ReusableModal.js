import React, { Component } from "react";
import PropTypes from "prop-types";
import { Modal, Grid, Divider, Button } from "semantic-ui-react";
import { BLUE, WHITE } from "../colors";


const { Row, Column } = Grid;


class ReusableModal extends Component {
  // ------------------------
  // ---- ON HANDLERS   ----
  // ------------------------
  closeModal() {
    this.props.onClose();
  }


  // ------------------------
  // ---- render methods ----
  // ------------------------

  renderContent() {
    const { mobile, title, actionLabel, children, noButtons } = this.props;
    return (
      <Grid centered>
        <Row centered style={{ paddingTop: 10, paddingBottom: 0 }}>
          <Column
            width={16}
            textAlign='center'
            style={{
              borderRadius: 10,
              color: 'black',
              fontFamily: 'bebas-neue',
              fontSize: mobile ? 30 : 25
            }}
          >
            <span style={{ color: BLUE }}>{title.charAt(0)}</span>{title.substr(1)}
          </Column>
        </Row>
        <Divider />
        <Row centered>
          <Column width={16}>
            {children}
          </Column>
        </Row>

        {noButtons ? null : (
          <Row centered>
          <Column width={16}>
            <div style={{ width: '100%', textAlign: 'right' }}>
              <Button
                  onClick={this.props.onCancel ? this.props.onCancel.bind() : this.closeModal.bind(this)}
                style={{ backgroundColor: WHITE, color: BLUE }}>
                  {this.props.cancelLabel ? this.props.cancelLabel : 'Cancelar'}
              </Button>
                {this.props.updateLabel && this.props.onUpdate ? (
                  <Button
                    onClick={this.props.onUpdate ? this.props.onUpdate.bind() : this.closeModal.bind(this)}
                    style={{ backgroundColor: WHITE, color: BLUE, paddingLeft: 0 }}>
                    {this.props.updateLabel ? this.props.updateLabel : 'Guardar'}
                  </Button>
                ) : null}
              <Button
                loading={this.props.loading}
                disabled={this.props.disabled}
                onClick={() => { this.props.onClick() }}
                style={{ backgroundColor: BLUE, color: WHITE, borderRadius: 15 }}>
                {actionLabel}
              </Button>
            </div>
          </Column>
        </Row>
        )}

      </Grid>
    )
  }


  renderClose() {
    return (
      <div
        style={{
          zIndex: 1000,
          position: "absolute",
          top: 10,
          right: 10,
          background: "rgb(230,230,230)",
          borderRadius: 20,
          cursor: "pointer",
          padding: 15
        }}
        onClick={this.closeModal.bind(this)}
      >
        <span
          style={{ fontSize: 18, position: "absolute", top: 1, left: 10 }}
        >
          x
        </span>
      </div>
    );
  }

  render() {
    const { visible, style } = this.props;

    return (
      <Modal
        className="hide-scroll"
        size="large"
        onClose={this.closeModal.bind(this)}
        open={visible}
        style={{
          width: 400,
          ...style,
        }}
      >
        <Modal.Content>
          {this.renderClose()}
          {this.renderContent()}
        </Modal.Content>
      </Modal>
    )
  }
}


ReusableModal.propTypes = {
  visible: PropTypes.bool,
  onClose: PropTypes.func,
  mobile: PropTypes.bool,
  title: PropTypes.string,
  actionLabel: PropTypes.string,
  children: PropTypes.any,
  style: PropTypes.object,
  onClick: PropTypes.func,
  disabled: PropTypes.bool,
  loading: PropTypes.bool,
  onCancel: PropTypes.func,
  cancelLabel: PropTypes.string,
  onUpdate: PropTypes.func,
  updateLabel: PropTypes.string,
  noButtons: PropTypes.bool
};

export default (ReusableModal);
