import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Radio } from 'semantic-ui-react';
import formatter from '../../../utils/formatter';
import { isEqual, eq } from 'lodash';
import i18n from '../utils/i18n.json';

const { errorRequired } = i18n.radio;


// crear componente
class RadioInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : '',
      valid,
      message: props.required ? errorRequired[props.language].replace('#label',props.label.toLowerCase()) : '',
      dirty: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!eq(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(e, data) {
    this.setState({ value: data.value, dirty: true, valid: true });
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    return this.state.value;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? errorRequired[this.props.language].replace('#label',this.props.label.toLowerCase()) : '',
      dirty: false,
    });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderRadios() {
    return this.props.options.map((radio, index) => (
      <Radio
        key={index}
        name={this.props.name}
        label={radio.text}
        value={`${radio.value}`}
        checked={this.state.value === radio.value}
        onChange={this.onChange.bind(this)}
        className={this.props.className}
        style={{ marginRight: 10, ...this.props.inputStyle }}
      />
    ));
  }

  render() {
    const labelString = this.props.required ? `${this.props.label} *` : this.props.label;

    if (this.props.inlineLabel) {
      return (
        <Form.Field>
          <span style={{ marginRight: 10, ...this.props.labelStyle }}>
            { labelString }
          </span>

          { this.renderRadios() }
        </Form.Field>
      );
    }

    return (
      <Form.Field>
        <label style={this.props.labelStyle}>
          { labelString }
        </label>

        { this.renderRadios() }
      </Form.Field>
    );
  }
}


RadioInput.propTypes = {
  label: PropTypes.string.isRequired,
  labelStyle: PropTypes.object,
  inlineLabel: PropTypes.bool,
  inputStyle: PropTypes.object,
  className: PropTypes.string,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  defaultValue: PropTypes.string,
  setFormData: PropTypes.func.isRequired,
  required: PropTypes.bool,
  language: PropTypes.string,
};


// exportar componente
export default RadioInput;
