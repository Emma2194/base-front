import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, TextArea, Label } from 'semantic-ui-react';
import { isEqual, eq } from 'lodash';
import formatter from '../../../utils/formatter';
import { FadeInView } from '../../FadeInView';
import i18n from '../utils/i18n.json';

const { errorRequired } = i18n.textArea;


class TextAreaInput extends Component {
  constructor(props) {
    super(props);
    let valid;

    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    this.state = {
      value: props.defaultValue ? props.defaultValue : '',
      valid,
      message: props.required ? errorRequired[props.language].replace('#label',props.label.toLowerCase()) : '',
      dirty: false,
      errorVisible: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      this.setState({ value: nextProps.defaultValue, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!eq(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(e) {
    this.setState({ errorVisible: false });

    if (this.props.required && !e.target.value) {
      this.setState({
        valid: false,
        dirty: true,
        value: '',
        message: errorRequired[this.props.language].replace('#label',this.props.label.toLowerCase()),
        errorVisible: this.props.showErrors,
      });
    } else if (this.props.max && e.target.value.length <= this.props.max) {
      this.setValue(e.target.value);
    } else if (!this.props.max) {
      this.setValue(e.target.value);
    }
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  setValue(value) {
    let valueData;
    this.setState({ dirty: true });

    // custom validations
    if (this.props.validator) {
      valueData = this.props.validator(value);

      this.setState({
        value,
        valid: valueData.valid,
        message: valueData.message ? valueData.message : '',
      });
    } else {
      this.setState({ value, valid: true, message: '' });
    }
  }

  getValue() {
    return this.state.value;
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;

    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    this.setState({
      value: this.props.defaultValue ? this.props.defaultValue : '',
      valid,
      message: this.props.required ? errorRequired[this.props.language].replace('#label',this.props.label.toLowerCase()) : '',
      dirty: false,
      errorVisible: false,
    });
  }

  showError() {
    this.setState({ errorVisible: true });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderErrorLabel() {
    const { errorVisible, message } = this.state;

    if (errorVisible) {
      return (
        <FadeInView style={styles.popUpContainer}>
          <Label basic pointing color="red" style={styles.popUpErrorLabel}>
            { message }
          </Label>
        </FadeInView>
      );
    }

    return null;
  }

  render() {
    const invalidInput = this.state.dirty && !this.state.valid;
    let className = '';
    let finalLabelStyle = { ...styles.label, ...this.props.labelStyle };
    let finalInputStyle = { ...styles.input, ...this.props.inputStyle };

    if (this.props.className) {
      className = `${this.props.className} ${invalidInput ? 'invalid' : 'valid'}`;
    } else {
      className = invalidInput ? 'invalid' : 'valid';
    }

    if (invalidInput) {
      finalLabelStyle = { ...this.props.labelStyle, ...styles.errorLabel };
      finalInputStyle = { ...this.props.inputStyle, ...styles.errorInput };
    }

    const labelString = this.props.required ? `${this.props.label} *` : this.props.label;

    return (
      <Form.Field>
        <label style={finalLabelStyle}>
          { labelString }
        </label>

        <TextArea
          value={this.state.value}
          name={this.props.name}
          className={className}
          onChange={this.onChange.bind(this)}
          disabled={this.props.readOnly}
          placeholder={this.props.placeholder}
          style={finalInputStyle}
        />

        { this.renderErrorLabel() }
      </Form.Field>
    );
  }
}


// estilos
const styles = {
  label: {

  },
  errorLabel: {
    color: '#9F3A38',
  },
  input: {

  },
  errorInput: {
    background: 'rgba(224, 180, 180, 0.48)',
    border: '1px solid #9F3A38',
    color: '#9F3A38',
  },
  popUpContainer: {
    position: 'absolute',
    top: 75,
    left: 0,
    zIndex: 100,
  },
  popUpErrorLabel: {
    fontSize: 13,
    textAlign: 'center',
    boxShadow: 'rgba(100, 100, 100, 0.25) 0px 2px 4px',
  },
};


TextAreaInput.propTypes = {
  placeholder: PropTypes.string.isRequired,
  readOnly: PropTypes.bool,
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  inputStyle: PropTypes.object,
  labelStyle: PropTypes.object,
  showErrors: PropTypes.bool,
  className: PropTypes.string,
  required: PropTypes.bool,
  defaultValue: PropTypes.string,
  validator: PropTypes.func,
  max: PropTypes.number,
  setFormData: PropTypes.func.isRequired,
  language: PropTypes.string,
};


// exportar componente
export default TextAreaInput;
