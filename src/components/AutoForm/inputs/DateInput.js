import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Form, Input, Label } from 'semantic-ui-react';
import moment from 'moment';
import { isEqual, eq } from 'lodash';
import formatter from '../../../utils/formatter';
import { FadeInView } from '../../FadeInView';
import i18n from '../utils/i18n.json';

const FORMAT_STRING = 'YYYY-MM-DD';
const { errorRequired } = i18n.date;


class DateInput extends Component {
  constructor(props) {
    super(props);

    let valid;
    if (props.defaultValue) {
      valid = true;
    } else {
      valid = !props.required;
    }

    let value = '';
    if (props.defaultValue) {
      value = moment(props.defaultValue).format(FORMAT_STRING);
    }

    this.state = {
      value,
      valid,
      message: props.required ? errorRequired[props.language].replace('#label',props.label.toLowerCase()) : '',
      dirty: false,
      errorVisible: false,
    };
  }


  // -----------------------------
  // ------ life cycle events ----
  // -----------------------------
  componentDidMount() {
    this.setForm();
  }

  UNSAFE_componentWillReceiveProps(nextProps) {
    if (!this.state.dirty && nextProps.defaultValue) {
      const value = moment(nextProps.defaultValue).format(FORMAT_STRING);
      this.setState({ value, valid: true });
    }
  }

  shouldComponentUpdate(nextProps, nextState) {
    const { clean } = formatter;
    return !isEqual(nextState, this.state) || !isEqual(clean(nextProps), clean(this.props));
  }

  componentDidUpdate(prevProps, prevState) {
    if (!eq(prevState, this.state)) {
      this.setForm();
    }
  }


  // -----------------------
  // ------ user events ----
  // -----------------------
  onChange(e) {
    this.setState({ value: e.target.value, dirty: true });

    // validar input
    if (this.props.required && !e.target.value) {
      this.setState({
        valid: false,
        message: errorRequired[this.props.language].replace('#label',this.props.label.toLowerCase()),
        errorVisible: this.props.showErrors,
      });
    } else {
      this.setState({
        valid: true,
        message: '',
        errorVisible: false,
      });
    }
  }

  setForm() {
    this.props.setFormData(this.props.name, this.state);
  }

  getValue() {
    if (this.state.value) {
      let date = new Date(this.state.value);
      date = moment(date).add(date.getTimezoneOffset(), 'minutes')._d;
      return this.props.format ? moment(date).format(this.props.format) : date;
    }

    return '';
  }

  dirtInput() {
    this.setState({ dirty: true });
  }

  resetInput() {
    let valid;
    if (this.props.defaultValue) {
      valid = true;
    } else {
      valid = !this.props.required;
    }

    let value = '';
    if (this.props.defaultValue) {
      value = moment(this.props.defaultValue).format(FORMAT_STRING);
    }

    this.setState({
      value,
      valid,
      message: this.props.required ? errorRequired[this.props.language].replace('#label',this.props.label.toLowerCase()) : '',
      dirty: false,
      errorVisible: false,
    });
  }

  showError() {
    this.setState({ errorVisible: true });
  }


  // --------------------------
  // ------ render methods ----
  // --------------------------
  renderErrorLabel() {
    const { errorVisible, message } = this.state;

    if (errorVisible) {
      return (
        <FadeInView style={styles.popUpContainer}>
          <Label basic pointing color="red" style={styles.popUpErrorLabel}>
            { message }
          </Label>
        </FadeInView>
      );
    }

    return null;
  }

  render() {
    const min = this.props.min ? moment(this.props.min).format(FORMAT_STRING) : '';
    const max = this.props.max ? moment(this.props.max).format(FORMAT_STRING) : '';
    const invalidInput = this.state.dirty && !this.state.valid;
    let className = '';

    let finalLabelStyle = { ...styles.label, ...this.props.labelStyle };
    let finalInputStyle = { ...styles.input, ...this.props.inputStyle };

    if (this.props.className) {
      className = `${this.props.className} ${invalidInput ? 'invalid' : 'valid'}`;
    } else {
      className = invalidInput ? 'invalid' : 'valid';
    }

    if (invalidInput) {
      finalLabelStyle = { ...this.props.labelStyle, ...styles.errorLabel };
      finalInputStyle = { ...this.props.inputStyle, ...styles.errorInput };
    }

    const labelString = this.props.required ? `${this.props.label} *` : this.props.label;

    // render input
    if (this.props.inlineLabel) {
      return (
        <Form.Field>
          <Input
            label={labelString}
            type="date"
            value={this.state.value}
            min={min}
            max={max}
            name={this.props.name}
            className={className}
            onChange={this.onChange.bind(this)}
            disabled={this.props.readOnly}
            style={finalInputStyle}
          />

          { this.renderErrorLabel() }
        </Form.Field>
      );
    }

    return (
      <Form.Field>
        <label style={finalLabelStyle}>
          { labelString }
        </label>

        <input
          type="date"
          value={this.state.value}
          min={min}
          max={max}
          name={this.props.name}
          className={className}
          onChange={this.onChange.bind(this)}
          disabled={this.props.readOnly}
          style={finalInputStyle}
        />

        { this.renderErrorLabel() }
      </Form.Field>
    );
  }
}


// estilos
const styles = {
  label: {

  },
  errorLabel: {
    color: '#9F3A38',
  },
  input: {

  },
  errorInput: {
    background: 'rgba(224, 180, 180, 0.48)',
    border: '1px solid #9F3A38',
    color: '#9F3A38',
  },
  popUpContainer: {
    position: 'absolute',
    top: 75,
    left: 0,
    zIndex: 100,
  },
  popUpErrorLabel: {
    fontSize: 13,
    textAlign: 'center',
    boxShadow: 'rgba(100, 100, 100, 0.25) 0px 2px 4px',
  },
};


DateInput.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired,
  className: PropTypes.string,
  min: PropTypes.instanceOf(Date),
  max: PropTypes.instanceOf(Date),
  defaultValue: PropTypes.oneOfType(
    [PropTypes.instanceOf(Date), PropTypes.string]
  ),
  format: PropTypes.string,
  showErrors: PropTypes.bool,
  required: PropTypes.bool,
  readOnly: PropTypes.bool,
  inlineLabel: PropTypes.bool,
  labelStyle: PropTypes.object,
  inputStyle: PropTypes.object,
  setFormData: PropTypes.func.isRequired,
  language: PropTypes.string,
};


// exportar componente
export default DateInput;
