
export const START_VIEW_TRANSITION = 'start-view-transition';
export const SHOW_NOTIFICATION = 'show-notification';

export const API_URL = 'una-url:3000';

export const MOBILE_BREAKPOINT = 768;
export const COOKIES_ACCEPTED = 'cookies-accepted';

export const ACCESS_TOKEN = 'access_token';
export const USER_DATA = 'userData';


