import moment from 'moment';
import 'moment/locale/es';

const formatter = {
  getNumberOfDay(date) {
    return (Date.UTC(date.getFullYear(), date.getMonth(), date.getDate()) - Date.UTC(date.getFullYear(), 0, 0)) / 24 / 60 / 60 / 1000;
  },
  getNumberOfWeek(date) {
    const tdt = new Date(date.valueOf());
    const dayn = (date.getDay() + 6) % 7;
    tdt.setDate(tdt.getDate() - dayn + 3);
    const firstThursday = tdt.valueOf();
    tdt.setMonth(0, 1);

    if (tdt.getDay() !== 4) {
      tdt.setMonth(0, 1 + ((4 - tdt.getDay()) + 7) % 7);
    }

    return 1 + Math.ceil((firstThursday - tdt) / 604800000);
  },
  date(date, formatString = 'D MMMM YYYY') {
    if (!date) return '';

    moment().localeData();
    return moment(date).format(formatString);
  },
  shortDate(date, formatString = 'D/MMM/YY') {
    if (!date) return '';

    moment.locale('en');
    return moment(date).format(formatString);
  },
  time(date, formatString = 'HH:mm') {
    if (!date) return '';

    moment.locale('en');
    return moment(date).format(formatString);
  },
  currency(number = 0) {
    const formattedNumber = `$${number.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')}`;
    const arrayNumber = formattedNumber.split('.');
    return arrayNumber[1] === '00' ? `${arrayNumber[0]}.00` : formattedNumber;
  },
  thousandSeparator(number = 0) {
    const formattedNumber = `${number.toFixed(2).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, '$1,')}`;
    const arrayNumber = formattedNumber.split('.');
    return arrayNumber[1] === '00' ? arrayNumber[0] : formattedNumber;
  },
  capitalize(text = '', type) {
    if (!text) return '';

    if (text && !text.length) {
      return text;
    }

    if (text.length === 1) {
      return text.toUpperCase();
    }

    if (type === 'sub') {
      return text;
    }

    const firstLetter = text.substring(0, 1);
    const remainingText = text.substring(1);
    return `${firstLetter.toUpperCase()}${remainingText.toLowerCase()}`;
  },
  applyMask(mask = '', number = '') {
    const maskArray = mask.split('');
    const numericValueArray = number.toString().split('');
    let maskedValue = '';
    let nextIndex = 0;

    maskArray.forEach((character) => {
      if (character === 'x') {
        maskedValue += numericValueArray[nextIndex];
        nextIndex += 1;
      } else {
        maskedValue += character;
      }
    });

    return maskedValue;
  },
  subString(text = '', maxLength = 50) {
    let shortString = text;

    if (shortString.length > maxLength) {
      shortString = `${shortString.substring(0, maxLength)}...`;
    }

    return shortString;
  },
  monthOfYear: (index = 0) => {
    const months = [
      'Enero',
      'Febrebro',
      'Marzo',
      'Abril',
      'Mayo',
      'Junio',
      'Julio',
      'Augosto',
      'Septiembre',
      'Octubre',
      'Noviembre',
      'Deciembre'
    ];

    return months[index]
  },
  monthOfYearShort: (index = 0) => {
    const months = [
      'Jan',
      'Feb',
      'Mar',
      'Apr',
      'May',
      'Jun',
      'Jul',
      'Aug',
      'Sep',
      'Oct',
      'Nov',
      'Dec'
    ];

    return months[index]
  }
};


export default formatter;
