import { combineReducers } from 'redux';
import appContainer from './app';
import webContainer from './web';
// import dashboardContainer from './dashboard';


const containers = {
  reducers: combineReducers({
    app: appContainer.reducer,
    web: webContainer.reducer,
    // dashboard: dashboardContainer.reducer,
  }),
  sagas: {
    web: webContainer.saga,
    // dashboard: dashboardContainer.saga,
  },
};


export default containers;
