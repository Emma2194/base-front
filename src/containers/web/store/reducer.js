import * as actionsTypes from './types';


const INITIAL_STATE = {
  selectedView: '',
  loadings: {},
};


const setLoading = (state, action) => {
  const { prop, value } = action.payload;
  const loadings = { ...state.loadings };

  loadings[prop] = value;
  return { ...state, loadings };
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionsTypes.SET_LOADING:
      return setLoading(state, action);
    case actionsTypes.SET_SELECTED_VIEW:
      return { ...state, selectedView: action.payload };



    default:
      return state;
  }
};
