import * as actionsTypes from './types';

export const setSelectedView = (params) => ({
  type: actionsTypes.SET_SELECTED_VIEW,
  payload: params,
});