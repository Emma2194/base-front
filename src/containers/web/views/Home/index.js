import { Grid } from 'semantic-ui-react';
import React, { Component } from 'react';
import { setSelectedView } from '../../store/actions';
import { Helmet } from "react-helmet";
import PropTypes from 'prop-types';
import { connect } from 'react-redux';


const { Column } = Grid;

const styles = {
  title: {
    marginTop: 30,
    borderRadius: 10,
    color: 'black',
    fontFamily: 'bebas-neue',
    fontSize: 35,
    borderButton: '1px Solid black'
  }
}
class HomeView extends Component {
  state = {};

  componentDidMount() {
    this.props.setSelectedView('home');
  }

  // -------------------
  // ----- methods -----
  // -------------------


  // ------------------------
  // ---- render methods ----
  // ------------------------



  render() {
    const { mobile } = this.props;
    const content = (
      <div style={{ marginTop: 30 }}>
        <Helmet>
          <title>Home</title>
        </Helmet>

        <Grid container={mobile ? false : true} style={{ paddingRight: 10, paddingLeft: 10 }} centered>
          <Column width={mobile ? 10 : 16} style={{ ...styles.title }}>
            HOME
          </Column>
        </Grid>
      </div>
    );

    if (mobile) {
      return content;
    }

    return (
      <div>
        <Grid container>
          <Column>
            {content}
          </Column>
        </Grid>
      </div>
    );
  }
}

HomeView.propTypes = {
  mobile: PropTypes.bool,
  setSelectedView: PropTypes.func,
};

const mapStateToProps = (state) => ({
});

const actions = {
  setSelectedView
};

export default connect(mapStateToProps, actions)(HomeView);
