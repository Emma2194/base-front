// import { Grid } from 'semantic-ui-react';
import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { BLUE } from '../../../../colors';


// const { Column } = Grid;

class Header extends Component {

  // -------------------
  // ----- methods -----
  // -------------------


  // ------------------------
  // ---- render methods ----
  // ------------------------

  render() {
    return (
      <div style={{ backgroundColor: BLUE, width: '100%' }}>
        header
      </div>
    )
  }
}

Header.propTypes = {
  // mobile: PropTypes.bool,
};

const mapStateToProps = (state) => ({
});

const actions = {
};

export default connect(mapStateToProps, actions)(Header);
