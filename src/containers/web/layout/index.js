import React, { Component, lazy, Suspense } from 'react';
import PropTypes from 'prop-types';
import { Grid, Segment, Dimmer, Loader } from 'semantic-ui-react';
import { Route, Switch } from "react-router-dom";
import { connect } from 'react-redux';
import eventManager from '../../../utils/eventManager';
import { FadeInOutView } from '../../../components';
import { MOBILE_BREAKPOINT } from '../../../utils/constants';
import { setMobile, setScrollPosition } from '../../app/store/actions';
import Header from './Header';
// import decorativeImage from '../../../assets/fondoImagen.png';



const HomeView = lazy(() => import('../views/Home'));


const { Row, Column } = Grid;

// const styles = {
//   decorativeImage: {
//     position: 'fixed',
//     width: '100%',
//     left: 0,
//     bottom: 0,
//   }
// };


class WebLayout extends Component {
  constructor(props) {
    super(props);

    this.state = {
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
      visibleSidebar: false,
    };
  }

  componentDidMount() {
    this.initNavigationListeners();
    this.setWindowDimensions();

    window.onresize = () => {
      this.setWindowDimensions();
    };

    window.onscroll = () => {
      const scrollPosition = window.pageYOffset;
      this.props.setScrollPosition(scrollPosition);
    };

  }

  componentWillUnmount() {
    eventManager.unsubscribe('navigate', this.navigateCallbackID);
  }


  // ---------------------
  // ----- methods -------
  // ---------------------
  setWindowDimensions() {
    this.setState({
      windowHeight: window.innerHeight,
      windowWidth: window.innerWidth,
    });

    if (window.innerWidth < MOBILE_BREAKPOINT) {
      this.props.setMobile(true);
    } else {
      this.props.setMobile(false);
    }
  }

  getMinHeight() {
    const { windowHeight } = this.state;
    return windowHeight;
  }

  navigate(url) {
    let eventType = 'start-navigation';

    eventManager.emit(eventType, () => {
      this.props.history.push(url);
    });
  }

  initNavigationListeners() {
    this.navigateCallbackID = eventManager.on('navigate', (url) => {
      this.navigate(url);
    });
  }

  // ----------------------------
  // ----- render methods -------
  // ----------------------------
  renderFallbackView() {
    return (
      <Grid container>
        <Column>
          <Segment style={{ height: 100, marginTop: 40 }}>
            <Dimmer active inverted>
              <Loader>Cargando</Loader>
            </Dimmer>
          </Segment>
        </Column>
      </Grid>
    );
  }


  renderViews() {
    const { mobile, scrollPosition, } = this.props;
    const navigate = this.navigate.bind(this);

    const commonProps = {
      navigate,
      mobile,
      scrollPosition,
      screen: {
        height: this.state.windowHeight,
        width: this.state.windowWidth
      }
    };

    let views;

    views = (
      <Suspense fallback={this.renderFallbackView()}>
        <Switch>
          {/* <Route path={`${this.props.match.url}/fichas`} render={(props) => <FichasView {...props} {...commonProps} />} /> */}
          <Route path="" render={(props) => <HomeView {...props} {...commonProps} navigate={navigate} />} />
        </Switch>
      </Suspense>
    );

    if (mobile) {
      return (
        <Row only="mobile" style={{ paddingBottom: 0 }}>
          <FadeInOutView eventType="start-navigation">
            <Grid style={{ margin: 0, marginTop: 55, marginBottom: 20 }}>
              <Column style={{ padding: 0 }}>
                {views}
              </Column>
            </Grid>
          </FadeInOutView>
        </Row>
      );
    }

    return (
      <Row only="tablet computer" style={{ minHeight: this.getMinHeight(), paddingBottom: 0 }}>
        <Column>
          <FadeInOutView eventType="start-navigation">
            {/* <Image src={decorativeImage} style={styles.decorativeImage} /> */}
            <Grid style={{ marginTop: 99, marginBottom: 60, padding: 0 }}>
              <Column style={{ padding: 0 }}>
                {views}
              </Column>
            </Grid>
          </FadeInOutView>
        </Column>
      </Row>
    );
  }

  render() {

    const mainComponent = (
      <div style={{ overflowX: 'hidden' }}>
        <Grid style={{ background: 'rgb(250,250,250)', margin: 0 }}>
          <Header />
          {this.renderViews()}
        </Grid>
      </div>
    );
    return mainComponent;
  }
}


WebLayout.propTypes = {
  history: PropTypes.object,
  match: PropTypes.object,


  mobile: PropTypes.bool,
  scrollPosition: PropTypes.number,

  //funciones
  setMobile: PropTypes.func,
  setScrollPosition: PropTypes.func,

};


const mapStateToProps = (state) => ({
  mobile: state.app.mobile,
  scrollPosition: state.app.scrollPosition,
});


const actions = {
  setMobile,
  setScrollPosition
};


export default connect(mapStateToProps, actions)(WebLayout);
