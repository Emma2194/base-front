import * as actionsTypes from './types';
import { MOBILE_BREAKPOINT } from '../../../utils/constants';


const INITIAL_STATE = {
  scrollPosition: 0,
  mobile: window.innerWidth < MOBILE_BREAKPOINT
};


export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case actionsTypes.SET_MOBILE:
      return { ...state, mobile: action.payload };
    case actionsTypes.SET_SCROLL_POSITION:
      return { ...state, scrollPosition: action.payload };
    default:
      return state;
  }
};
