import * as actionsTypes from './types';

export const setMobile = (params) => ({
  type: actionsTypes.SET_MOBILE,
  payload: params,
});

export const setScrollPosition = (params) => ({
  type: actionsTypes.SET_SCROLL_POSITION,
  payload: params,
});

