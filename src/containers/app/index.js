import reducer from './store/reducer';

const container = {
  reducer,
};

export default container;
