import React, { Component, lazy, Suspense } from 'react';
import { Provider } from 'react-redux';
import { Image } from 'semantic-ui-react';
import NotificationSystem from 'react-notification-system';
import configureStore from '../configureStore';
import eventManager from '../utils/eventManager';
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { SHOW_NOTIFICATION } from '../utils/constants';
import { WHITE } from '../colors';
import loader from '../assets/main-loader.gif';

// const DashboardLayout = lazy(() => import('../containers/dashboard/layout'));
const WebLayout = lazy(() => import('../containers/web/layout'));

const store = configureStore();


class App extends Component {
  componentDidMount() {
    this.callbackID = eventManager.on(SHOW_NOTIFICATION, (data) => {
      if (this.notificationSystem) {
        this.notificationSystem.addNotification({
          level: data.type,
          message: data.message,
        });
      }
    });
  }

  componentWillUnmount() {
    eventManager.unsubscribe(SHOW_NOTIFICATION, this.callbackID);
  }

  renderFallbackView() {
    return (
      <div style={{ background: '#1A1A1A', color: WHITE, height: window.innerHeight, width: '100%', position: 'relative' }}>
        <div style={{ height: '100%', display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center' }}>
          <Image
            src={loader}
            style={{ width: 200, height: 200 }}
          />
        </div>
      </div>
    );
  }

  render() {
    return (
      <Provider store={store}>
        <Router>
          <Suspense fallback={this.renderFallbackView()}>
            <Switch>
              <Route path="/" render={(props) => <WebLayout {...props} />} />
            </Switch>
          </Suspense>

          <NotificationSystem ref={(ref) => { this.notificationSystem = ref; }} />
        </Router>
      </Provider>
    );
  }
}


export default App;
